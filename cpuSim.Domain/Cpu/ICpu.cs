﻿namespace cpuSim.Domain.Cpu
{
    public interface ICpu
    {
        bool IsInInterruptStage { get; }
        bool IsRunning { get;  }

        void Initialize();
        void SetExternalInterrupt();
        void SetProgramCounterToFirstAdress();
    }
}