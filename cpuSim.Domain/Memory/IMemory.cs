﻿namespace cpuSim.Domain.Memory
{
    public interface IMemory
    {
        void Initialize();
    }
}