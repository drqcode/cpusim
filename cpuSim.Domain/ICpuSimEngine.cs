﻿using cpuSim.Domain.Console;
using cpuSim.Domain.Cpu;
using cpuSim.Domain.Loader;
using cpuSim.Domain.Memory;
using DrqMvc.Model;

//using DrqMvc.Model;

namespace cpuSim.Domain
{
    public interface ICpuSimEngine : IModel
    {
        IMemory Memory { get; set; }
        IConsole Console { get; set; }
        IProgramLoader ProgramLoader { get; set; }
        ICpu Cpu { get; set; }
        
        void Initialize();
        void Clear();
        void Run();
        void Stop();
        void Debug();
        void RaiseExternalInterrupt();

        bool IsInInterruptHandler { get; }
        bool IsRunning { get; }
    }
}