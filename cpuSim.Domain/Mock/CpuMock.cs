﻿using System;
using cpuSim.Domain.Cpu;

namespace cpuSim.Domain.Mock
{
    internal class CpuMock : ICpu
    {
        private bool _interrupt;
        private bool _isInitialized;
        private bool _isRunning;

        public bool IsInInterruptStage => _interrupt;

        public bool IsRunning => _isRunning;

        public void Initialize()
        {
            _isInitialized = true;
        }

        public void SetExternalInterrupt()
        {
            _interrupt = true;
        }

        public void SetProgramCounterToFirstAdress()
        {
            _isRunning = true;
        }
    }
}