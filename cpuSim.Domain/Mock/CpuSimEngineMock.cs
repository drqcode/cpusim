﻿using System;
using cpuSim.Domain.Console;
using cpuSim.Domain.Cpu;
using cpuSim.Domain.Loader;
using cpuSim.Domain.Memory;
using DrqMvc.Model;
using DrqMvc.ModelEventHandling;


namespace cpuSim.Domain.Mock
{
    public class CpuSimEngineMock : ICpuSimEngine
    {
        private IMemory _memory;
        private ICpu _cpu;
        private IProgramLoader _loader;
        private IConsole _console;

        public event ModelHandler<IModel> Changed;

        public IMemory Memory
        {
            get
            {
                return _memory;
            }

            set
            {
                _memory = value;
            }
        }

        public ICpu Cpu
        {
            get
            {
                return _cpu;
            }

            set
            {
                _cpu = value;
            }
        }

        public IProgramLoader ProgramLoader
        {
            get
            {
                return _loader;
            }

            set
            {
                _loader = value;
            }
        }

        public IConsole Console
        {
            get
            {
                return _console;
            }

            set
            {
                _console = value;
            }
        }

        public bool IsInInterruptHandler => _cpu.IsInInterruptStage;

        public bool IsRunning => _cpu.IsRunning;

        public CpuSimEngineMock()
        {
            //mock
            _memory = new MemoryMock();
            _cpu = new CpuMock();
            _loader = new ProgramLoaderMock();
            _console = new ConsoleMock();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public void Debug()
        {
            throw new NotImplementedException();
        }

        public void Initialize()
        {
            _memory.Initialize();
            _cpu.Initialize();
        }

        public void RaiseExternalInterrupt()
        {
            _cpu.SetExternalInterrupt();
        }

        public void Run()
        {
        }

        public void Stop()
        {
        }
    }
}