﻿namespace cpuSim.GUI
{
    public class CpuSimGui
    {
        private FormRepository _forms;

        public CpuSimGui()
        {
            Initialize();
        }

        public void Show()
        {
            _forms.Main.Show();
        }

        private void Initialize()
        {
            _forms = new FormRepository();
        }
    }
}