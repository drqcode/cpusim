﻿using cpuSim.GUI.Forms;

namespace cpuSim.GUI
{
    internal class FormRepository
    {
        public MainForm Main = new MainForm();
        public AboutForm About = new AboutForm();
        public ConsoleForm Console = new ConsoleForm();
        public AssemblerForm Assembler = new AssemblerForm();
    }
}