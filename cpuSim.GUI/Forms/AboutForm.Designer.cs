﻿namespace cpuSim.GUI.Forms
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelAboutButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.copyRightLabel = new System.Windows.Forms.Label();
            this.aboutLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cancelAboutButton
            // 
            this.cancelAboutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelAboutButton.Location = new System.Drawing.Point(116, 242);
            this.cancelAboutButton.Name = "cancelAboutButton";
            this.cancelAboutButton.Size = new System.Drawing.Size(64, 23);
            this.cancelAboutButton.TabIndex = 0;
            this.cancelAboutButton.Text = "Cancel";
            this.cancelAboutButton.UseVisualStyleBackColor = true;
            this.cancelAboutButton.Click += new System.EventHandler(this.cancelAboutButton_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.aboutLabel);
            this.flowLayoutPanel1.Controls.Add(this.copyRightLabel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(75, 75, 75, 5);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(296, 224);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // copyRightLabel
            // 
            this.copyRightLabel.AutoSize = true;
            this.copyRightLabel.Location = new System.Drawing.Point(78, 160);
            this.copyRightLabel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.copyRightLabel.Name = "copyRightLabel";
            this.copyRightLabel.Size = new System.Drawing.Size(114, 13);
            this.copyRightLabel.TabIndex = 0;
            this.copyRightLabel.Text = "© Capatina Constantin";
            // 
            // aboutLabel
            // 
            this.aboutLabel.AutoSize = true;
            this.aboutLabel.Location = new System.Drawing.Point(78, 75);
            this.aboutLabel.Name = "aboutLabel";
            this.aboutLabel.Size = new System.Drawing.Size(140, 65);
            this.aboutLabel.TabIndex = 1;
            this.aboutLabel.Text = "texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttextt" +
    "exttexttexttexttexttexttexttexttexttexttexttext";
            // 
            // AboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelAboutButton;
            this.ClientSize = new System.Drawing.Size(296, 384);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.cancelAboutButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AboutForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cancelAboutButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label copyRightLabel;
        private System.Windows.Forms.Label aboutLabel;
    }
}