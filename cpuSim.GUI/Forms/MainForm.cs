﻿using System;
using System.Windows.Forms;

namespace cpuSim.GUI.Forms
{
    public partial class MainForm : Form
    {
        private ConsoleForm _consoleWindow;

        public MainForm()
        {
            InitializeComponent();
            _consoleWindow = new ConsoleForm();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new AboutForm().ShowDialog();
        }

        private void loadProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AssemblerForm().ShowDialog();
        }

        private void showConsoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _consoleWindow.Show();
        }
    }
}