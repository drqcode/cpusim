﻿using System;
using System.Windows.Forms;

namespace cpuSim.GUI.Forms
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
        }

        private void cancelAboutButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}