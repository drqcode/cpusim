﻿using System;
using cpuSim.Domain;
using cpuSim.Domain.Mock;
using cpuSim.GUI;
using DrqMvc.Controller;
//using DrqMvc.Controller;

namespace cpuSim
{
    public class CpuSim
    {
        private CpuSimGui _gui;
        private ICpuSimEngine _engine;

        public CpuSim()
        {
            Initialize();

            _gui.Show();
        }

        private void Initialize()
        {
            _gui = new CpuSimGui();
            _engine = new CpuSimEngineMock();
        }

        private void BuildControllers()
        {
            
        }

        private interface IControllerBuilder
        {
            IController Build();
        }

        private class MockControllerBuilder : IControllerBuilder
        {
            public IController Build()
            {
                throw new NotImplementedException();
            }
        }
    }
}