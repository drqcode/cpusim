﻿using System.Windows.Forms;

namespace cpuSim
{
    internal class CpuSimApplicationContext : ApplicationContext
    {
        public CpuSimApplicationContext()
        {
            new CpuSim();
        }
    }
}